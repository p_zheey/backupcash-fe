import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import 'antd/dist/antd.css';
import SfsDeposit from "./pages/SfsDeposit";
import SFSAuthorization from './pages/SFSAuthorization';
import CreateAccountPin from './pages/CreateAccountPin';
import WithdrawalPin from './pages/WithdrawalPin';

class App extends Component {
    render() {
        return (
            <div>
            <Switch>
                <Route exact path={'/deposit'} component={SfsDeposit}/>
                <Route exact path={'/authorize'} component={SFSAuthorization}/>
                <Route exact path={'/account'} component={CreateAccountPin}/>
                <Route exact path={'/withdrawal'} component={WithdrawalPin}/>
            </Switch>
        </div>
    );
    }
}

export default App;
