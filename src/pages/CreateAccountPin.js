import React, {Component} from 'react';
import axios from "axios/index";
import queryString from 'query-string';
import logo from '../backupcash.jpg'

export default class CreateAccountPin extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
            first_name: '',
            last_name:'',
            phone_number: '',
            loading: false,
            withdrawal_pin: ""
        }
    }

    componentDidMount() {
        const query = queryString.parse(this.props.location.search);

        this.setState({email: query.email, first_name: query.first_name,
            last_name: query.last_name, phone_number: query.phone_number, messenger_id: query.id})
        console.log("query", query)
        
    }

    createAccount = () =>{
        this.setState({loading: true})
        const {last_name, email, first_name, phone_number, id, withdrawal_pin } = this.state

        const data  = {user_last_name: last_name, user_email: email, 
        user_first_name:first_name, user_phone_number: phone_number,
            withdrawal_pin: withdrawal_pin, messenger_id: id};

        return axios.post(`https://backupcash-middleware.herokuapp.com/account/create-account`, data)
            .then(resp => {
                console.log(resp)
                if(resp.data.error){
                    if(JSON.parse(JSON.stringify(resp.data.error.errors)).hasOwnProperty("email")){
                        window.open("https://m.me/BackUpCash?ref=Enter%20correct%20email", "_self")
                     
                    }else if(JSON.parse(JSON.stringify(resp.data.error.errors)).hasOwnProperty("facebook_messenger_id")){
                       window.open("https://m.me/BackUpCash?ref=Old%20user", "_self")
                    }else if(JSON.parse(JSON.stringify(resp.data.error.errors)).hasOwnProperty("phone")){
                        window.open("https://m.me/BackUpCash?ref=Enter%20correct%20phone", "_self")
                    }else {
                       window.open("https://m.me/BackUpCash?ref=Transaction%20failed", "_self")
                    }
                }else{
                   window.open("https://m.me/BackUpCash?ref=Registration%20Success", "_self")
                }
            })
            .catch(err => {
                
                   window.open("https://m.me/BackUpCash?ref=Account%20creation%20failed", "_self")
            });
    }

    handleInput = (e) => {
        this.setState({withdrawal_pin: e.target.value})
    }

    render() {
        return (
            <div>
                {this.state.loading? <div>redirecting...</div>:<div className="page">
                <div className="z-logo">
                    <img src = {logo} alt="logo"/>
                </div>
                <div>
                    <p className="z-p">Please enter your pin</p>
                </div>
                <div>
                    <input  className="z-input" onChange={e => {this.handleInput(e)}}/>
                    <button className="button" onClick={e => {this.createAccount()}} disabled={this.state.withdrawal_pin.length < 1}> Proceed </button>
                </div>
                </div>}
            </div>
        )
    }
}