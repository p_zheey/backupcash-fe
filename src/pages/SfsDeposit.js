import React, {Component} from 'react';
import axios from "axios/index";
import queryString from 'query-string';

export const symphony = "pk_live_892c43a50fcbaf17a0e0ac6010a35a2c07de7f83"
export const symphony_test = "pk_test_a59d1204944c01bf05330ab59fb1abe607eb36a6"
export default class SfsDeposit extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
            amount: '',
            referenceId:'',
            userId: '',
            orderId: '',
            loading: false,
            phone: ""
        }
    }

    componentDidMount() {
        const query = queryString.parse(this.props.location.search);

        this.setState({email: query.email, amount: query.amount,
            referenceId: query.reference, userId: query.messengerId, phone: query.phone})
        console.log("query", query)
        window.payWithPaystack(symphony,
            query.email, query.amount, "NGN", this.createLog, query.reference)
    }

    createLog = (data) =>{
        this.setState({loading: true})

        const{userId, phone} = this.state;
        return axios.post(`https://backupcash-middleware.herokuapp.com/account/log`, { ref: data.reference,
            facebook_messenger_id: userId,user_phone_number:phone, type: "quick"})
            .then(resp => {
                if(resp.data) {
                    window.open("https://m.me/BackUpCash?ref=Deposit%20Success", "_self")
                }
            })
            .catch(err => {
                window.open("https://m.me/BackUpCash?ref=Deposit%20Failure", "_self")
            });
    }

    render() {
        return (
            <div>
                {this.state.loading? <div>redirecting...</div>:<div></div>}
            </div>
        )
    }
}