import React, {Component} from 'react';
import axios from "axios/index";
import queryString from 'query-string';
import logo from '../backupcash.jpg'

export default class WithdrawalPin extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
            first_name: '',
            last_name:'',
            phone_number: '',
            loading: false,
            withdrawal_pin: ""
        }
    }

    componentDidMount() {
        const query = queryString.parse(this.props.location.search);

        
        this.setState({bank_account: query.bn, withdraw_amount: query.wa, withdrawal_date: query.wd,penalty_from: query.pf,
            phone_number: query.phone_number, messenger_id: query.id, withdrawal_point: query.wp})        
    }

    withdraw = () =>{
        this.setState({loading: true})
        const {bank_account, withdraw_amount, withdrawal_date, penalty_from, phone_number,messenger_id, withdrawal_pin, withdrawal_point } = this.state

        const data  = {bank_account, withdraw_amount, withdrawal_date, penalty_from,
            user_phone_number:phone_number, messenger_id, withdrawal_point, withdrawal_pin};

        return axios.post(`https://backupcash-middleware.herokuapp.com/account/withdraw`, data)
            .then(resp => {
                console.log(resp)
                    if(resp.data === "Withdrawal Success"){
                        window.open("https://m.me/BackUpCash?ref=Withdrawal%20Success", "_self")
                     
                    }else if(resp.data === "Withdrawal pin mismatch"){
                       window.open("https://m.me/BackUpCash?ref=Pin%20Mismatch", "_self")
                    }else if(resp.data === "Insufficient amount"){
                        window.open("https://m.me/BackUpCash?ref=Insufficient%20amount", "_self")
                    }else if(resp.data === "Withdrawal amount small"){
                        window.open("https://m.me/BackUpCash?ref=Withdrawal%20amount%20small", "_self")
                    }else {
                       window.open("https://m.me/BackUpCash?ref=Failed%20withdrawal", "_self")
                    }
                
            })
            .catch(err => {
                
                   window.open("https://m.me/BackUpCash?ref=Failed%20withdrawal", "_self")
            });
    }

    handleInput = (e) => {
        this.setState({withdrawal_pin: e.target.value})
    }

    render() {
        return (
            <div>
                {this.state.loading? <div>redirecting...</div>:<div className="page">
                <div className="z-logo">
                    <img src = {logo} alt="logo"/>
                </div>
                <div>
                    <p className="z-p">Please enter your pin</p>
                </div>
                <div>
                    <input  className="z-input" onChange={e => {this.handleInput(e)}}/>
                    <button className="button" onClick={e => {this.withdraw()}} disabled={this.state.withdrawal_pin.length < 1}> Proceed </button>
                </div>
                </div>}
            </div>
        )
    }
}